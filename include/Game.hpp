#pragma once
#include <array>

#define FRAME_HEIGHT 84
#define FRAME_WIDTH 48


class Game {

public:
    static Game& getInstance();
    
    Game(Game const&) = delete;
    void operator=(Game const&) = delete;

    volatile int frame_buffer[FRAME_WIDTH][FRAME_HEIGHT];    
private:
    Game();
};