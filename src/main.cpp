#include <cstdlib>
#include <vector>

#include <pin.h>
#include <Game.hpp>

void* __dso_handle;


#define delay(x)      SysCtlDelay(SysCtlClockGet() / 3 * x);

int main(void)
{
    // Clock (80MHz)
    SysCtlClockSet(SYSCTL_SYSDIV_2_5 | SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ | SYSCTL_OSC_MAIN);

    Game::getInstance().frame_buffer;

    volatile int test = Game::getInstance().frame_buffer[0][1];    
    // Setup Red Led
    Pin led = Pin(PF1);
    led.setDir(OUT);

    while(1){
        led.toggle();
        delay(0.1);
    }
}
