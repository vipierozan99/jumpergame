#include "Game.hpp"

Game::Game() {
    for(auto &col : this->frame_buffer){
        for(volatile int &pixel : col){
            pixel = 6;
        }
    }
}

Game& Game::getInstance() {
    static Game instance;
    return instance;
}