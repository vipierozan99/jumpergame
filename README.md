# jumpergame

SETUP


# Get dependencies:
  - sudo apt install openocd lm4flash
  # For 64-bit systems:
  - sudo dpkg –add-architecture i386
  
  # For everybody:
  - sudo apt-get update
  - sudo apt-get install flex bison libgmp3-dev libmpfr-dev \
      libncurses5-dev libmpc-dev autoconf texinfo build-essential \
      libftdi-dev python-yaml zlib1g-dev libtool
  
  # For 64-bit systems yet again:
  - sudo apt-get install libc6:i386 libncurses5:i386 libstdc++6:i386
  
  # I needed these as well flashing over USB:
  sudo apt-get install libusb-1.0-0 libusb-1.0-0-dev

# Then get the eabi arm none toolchain:

  - https://launchpad.net/gcc-arm-embedded/+download
  # Build it
  - make


 - Export it toolchain bin folder to PATH
 - Edit ~/.bashrc for persistance

# On parent folder there must be a TivaWare folder
 # Get it 
 - http://software-dl.ti.com/tiva-c/SW-TM4C/latest/index_FDS.html
 # Unzip it
 - unzip SW-TM4C-2.1.0.12573.exe
 # Build it
 - make

# Interesting stuff

 - https://arobenko.gitbooks.io/bare_metal_cpp/content/compiler_output/dyn_mem.html
