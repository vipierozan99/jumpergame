/*
 * Game.cpp
 *
 *  Created on: 23 de out de 2019
 *      Author: root
 */

#include <Game.h>
#include <Player.h>
#include <vector>

Game* Game::GameInstance = NULL;

Game* Game::instance(){
    if(!Game::GameInstance)
        Game::GameInstance = new Game(84,48);
    return Game::GameInstance;
}

Game::Game(int screen_height, int screen_width)
{
    int **frame= new int*[screen_height];
    int row,col;
    for(row=0;row<screen_height;row++){
        frame[row] = new int[screen_width];
    }

    for(row=0;row<screen_height;row++){
        for(col=0;col<screen_width;col++){
            frame[row][col] = 24;
        }
    }

    this->test = 666;
    this->frame = frame;
    this->hero = Player();
}

Game::~Game()
{
    int row;
    for(row=0;row<this->screen_height;row++)
        delete this->frame[row];
    delete this->frame;
}

