/*
 * HardwareController.cpp
 *
 *  Created on: 23 de out de 2019
 *      Author: root
 */

#include <vector>
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_ints.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/uart.h"
#include "driverlib/pin_map.h"
#include "driverlib/interrupt.h"
#include <HardwareController.h>
#include <Game.h>

using namespace std;

HardwareController::HardwareController(Game* game)
{
    // TODO Auto-generated constructor stub
    this->game=game;
    SysCtlClockSet(SYSCTL_SYSDIV_2_5|SYSCTL_USE_PLL|SYSCTL_OSC_MAIN|SYSCTL_XTAL_16MHZ); // 80 MHz //passo 1
    this->clock=SysCtlClockGet();
    IntMasterEnable();
    this->initializeScreen();
}

void HardwareController::initializeScreen(){
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0); //passo 2
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA); //passo 3
    GPIOPinTypeUART(GPIO_PORTA_BASE,GPIO_PIN_0 | GPIO_PIN_1); //Configura pin (s) para uso do periférico UART. //passo 6
    GPIOPadConfigSet(GPIO_PORTA_BASE,GPIO_PIN_0 | GPIO_PIN_1,GPIO_STRENGTH_2MA,GPIO_PIN_TYPE_STD); //passo 5
    GPIOPinConfigure(GPIO_PA0_U0RX); //passo 4
    GPIOPinConfigure(GPIO_PA1_U0TX); //passo 4
    UARTConfigSetExpClk(UART0_BASE,this->clock,115200,(UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE)); //passo 7
    UARTEnable(UART0_BASE); //passo 8
    UARTIntEnable(UART0_BASE,UART_INT_RX | UART_INT_RT);// Podemos habilitar a interrupção por Timeout //passo 9
    GameInstance;
    UARTIntRegister(UART0_BASE,nullptr);
    IntEnable(INT_UART0);
    UARTFIFOLevelSet(UART0_BASE, UART_FIFO_TX1_8, UART_FIFO_RX1_8);
    UARTFIFOEnable(UART0_BASE);
}

HardwareController::~HardwareController()
{
    // TODO Auto-generated destructor stub
}

