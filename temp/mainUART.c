#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_ints.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/uart.h"
#include "driverlib/pin_map.h"
#include "driverlib/interrupt.h"

char command_buffer[256];
int command_cursor=0;

//LAB07
void Handler_UART0(void){
    UARTCharPut(UART0_BASE, UARTCharGet(UART0_BASE)); //echo - Função bloqueante
    UARTIntClear(UART0_BASE,UART_INT_RX | UART_INT_RT);
}

void UARTRespond(char *str,int size){
    int i;
    for(i=0;i<size;i++){
        UARTCharPut(UART0_BASE,str[i]);
    }
    UARTCharPut(UART0_BASE,'\n');
}

void trataUART0(void)
{
    UARTIntClear(UART0_BASE, UART_INT_RX);
    char ui8;
    ui8=UARTCharGet(UART0_BASE);
    command_buffer[command_cursor] = ui8;
    command_cursor++;
    if(ui8==0x0D){
        UARTRespond(command_buffer,command_cursor);
        command_cursor=0;
    }

//    UARTCharPut(UART0_BASE, ui8);
}

int main(void)
{
    SysCtlClockSet(SYSCTL_SYSDIV_2_5|SYSCTL_USE_PLL|SYSCTL_OSC_MAIN|SYSCTL_XTAL_16MHZ); // 80 MHz //passo 1
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0); //passo 2
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA); //passo 3
    GPIOPinTypeUART(GPIO_PORTA_BASE,GPIO_PIN_0 | GPIO_PIN_1); //Configura pin (s) para uso do periférico UART. //passo 6
    GPIOPadConfigSet(GPIO_PORTA_BASE,GPIO_PIN_0 | GPIO_PIN_1,GPIO_STRENGTH_2MA,GPIO_PIN_TYPE_STD); //passo 5
    GPIOPinConfigure(GPIO_PA0_U0RX); //passo 4
    GPIOPinConfigure(GPIO_PA1_U0TX); //passo 4
    UARTConfigSetExpClk(UART0_BASE,SysCtlClockGet(),115200,(UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE)); //passo 7
    UARTEnable(UART0_BASE); //passo 8
    UARTIntEnable(UART0_BASE,UART_INT_RX | UART_INT_RT);// Podemos habilitar a interrupção por Timeout //passo 9
    UARTIntRegister(UART0_BASE,trataUART0);
    IntEnable(INT_UART0); //passo 10
    IntMasterEnable(); //passo 11

    //configurando o fifo
    UARTFIFOLevelSet(UART0_BASE, UART_FIFO_TX1_8, UART_FIFO_RX1_8);
    UARTFIFOEnable(UART0_BASE);

    while(1)
    {
    }
}


