/*
 * Player.h
 *
 *  Created on: 23 de out de 2019
 *      Author: root
 */

#ifndef PLAYER_H_
#define PLAYER_H_

#include <vector>

using namespace std;

class Player
{
    int pos_x;
    int pos_y;
    int speed_x;
    int speed_y;
    int accel_x;
    int accel_y;
public:
    Player();
    virtual ~Player();
    void setPos(vector<int>);
    vector<int> getPos();
};

#endif /* PLAYER_H_ */
