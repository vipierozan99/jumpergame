/*
 * Game.h
 *
 *  Created on: 23 de out de 2019
 *      Author: root
 */

#ifndef GAME_H_
#define GAME_H_

#include <vector>
#include <Player.h>

using namespace std;

class Game
{
    Player hero;
    int screen_width;
    int screen_height;
    Game(int,int);
    static Game* GameInstance;
public:
    static Game* instance();
    int **frame;
    int test;
    virtual ~Game();
    void onBtnPress(int);
};

extern Game GameInstance;


#endif /* GAME_H_ */
