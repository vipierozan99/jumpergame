/*
 * Singleton.h
 *
 *  Created on: 23 de out de 2019
 *      Author: root
 */

#ifndef SINGLETON_H_
#define SINGLETON_H_

class Singleton
{
public:
    Singleton();
    virtual ~Singleton();
};

#endif /* SINGLETON_H_ */
