/*
 * Player.cpp
 *
 *  Created on: 23 de out de 2019
 *      Author: root
 */

#include <Player.h>
#include <vector>
using namespace std;

Player::Player()
{
    // TODO Auto-generated constructor stub
    this->pos_x = 0;
    this->pos_y = 0;
    this->speed_x = 0;
    this->speed_y=0;
    this->accel_x=0;
    this->accel_y=0;
}

Player::~Player()
{
    // TODO Auto-generated destructor stub
}

void Player::setPos(vector<int> pos){
    this->pos_x = pos[0];
    this->pos_y = pos[1];
}

vector<int> Player::getPos(){
    return vector<int>{this->pos_x,this->pos_y};
}
