#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_ints.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/uart.h"
#include "driverlib/pin_map.h"
#include "driverlib/interrupt.h"

#include <Player.h>
#include <Game.h>
#include <HardwareController.h>

#define FRAME_WIDTH 84
#define FRAME_HEIGHT 48
char frame[FRAME_HEIGHT][FRAME_WIDTH];
int player_x=5, player_y=5;

void resetCursor(void){
    UARTCharPut(UART0_BASE, 27);
    UARTCharPut(UART0_BASE, 27);
    UARTCharPut(UART0_BASE, '[');
//    UARTCharPut(UART0_BASE, '[');
    UARTCharPut(UART0_BASE, 'H');
    UARTCharPut(UART0_BASE, '\r');
//    UARTCharPut(UART0_BASE, '^');
//    UARTCharPut(UART0_BASE, 27);
}

void clearScreen(void){
    UARTCharPut(UART0_BASE, 27);
    UARTCharPut(UART0_BASE, 27);
    UARTCharPut(UART0_BASE, 'J');
}

void drawFrame(void){
    resetCursor();
    int x,y,c;
    for(y=0;y<FRAME_HEIGHT;y++){
        for(x=0;x<FRAME_WIDTH;x++){
            UARTCharPut(UART0_BASE, frame[y][x]);
//            for(c=0;c<800000;c++){}
        }
        UARTCharPut(UART0_BASE, '\r');
        if(y!=FRAME_HEIGHT-1)
        UARTCharPut(UART0_BASE, '\n');
    }
//    int counter;
//    for(counter=0;counter<10000000;counter++){}
//    UARTCharPut(UART0_BASE, '\n');
//    UARTCharPut(UART0_BASE, '\r');
//    clearScreen();
}

void trataUART0(void)
{
    UARTIntClear(UART0_BASE, UART_INT_RX | UART_INT_RT);
    int32_t input = UARTCharGet(UART0_BASE);
//    frame[0][0] = input;
    switch(input){
    case 'D':
        player_x--;
        break; //change move funct later, this trails
    case 'A':
        player_y--;
        break;
    case 'C':
        player_x++;
        break;
    case 'B':
        player_y++;
        break;
    }
    frame[player_y][player_x] = 'o';
}


int main(void)
{
    int u=13;
    u++;
    Game *MainGame = Game::instance();
    int* j = MainGame->frame[0];
    int kk = j[5];
    int t = MainGame->test;

    HardwareController hardwCtl(MainGame);

    Player hero = Player();
    while(1)
    {
        frame[player_y][player_x] = 'X';
//        resetCursor();
//        clearScreen();
        drawFrame();
//        int counter;
//        for(counter=0;counter<10000000;counter++){}
    }
}







