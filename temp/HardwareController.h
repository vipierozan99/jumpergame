/*
 * HardwareController.h
 *
 *  Created on: 23 de out de 2019
 *      Author: root
 */

#ifndef HARDWARECONTROLLER_H_
#define HARDWARECONTROLLER_H_
#include <vector>
#include <stdint.h>
#include <Game.h>

using namespace std;


class HardwareController
{
    uint32_t clock;
    Game* game;
public:
    HardwareController(Game*);
    virtual ~HardwareController();
    void drawFrame(vector<vector<int>>);
    void initializeScreen(void);
    void SWHandler(void);

};

#endif /* HARDWARECONTROLLER_H_ */
