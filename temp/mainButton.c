#include "stdint.h"
#include "stdbool.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_ints.h"
#include "driverlib/debug.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/systick.h"
#include "driverlib/pwm.h"
#include "driverlib/interrupt.h"
#include "driverlib/timer.h"

uint32_t COLOR = GPIO_PIN_3;
uint32_t BUTTON = GPIO_PIN_4;

void toggleLED(void){
    GPIOIntClear(GPIO_PORTF_BASE, GPIOIntStatus(GPIO_PORTF_BASE, true));
    GPIOIntDisable(GPIO_PORTF_BASE, BUTTON);
    TimerEnable(TIMER0_BASE, TIMER_A);
    uint32_t state = GPIOPinRead(GPIO_PORTF_BASE, COLOR);
    uint32_t toggle = (state==COLOR)? 0x00:0xFF;
    GPIOPinWrite(GPIO_PORTF_BASE, COLOR, toggle);
}

void enableButtonInt(void){
    TimerIntClear(TIMER0_BASE, TimerIntStatus(TIMER0_BASE,true));
//    GPIOIntClear(GPIO_PORTF_BASE, GPIOIntStatus(GPIO_PORTF_BASE, true));
    GPIOIntEnable(GPIO_PORTF_BASE, BUTTON);
    return;
}


int main(void)
{
    int i=0;

    SysCtlClockSet(SYSCTL_SYSDIV_2 | SYSCTL_USE_PLL | SYSCTL_OSC_INT);//80MHz
    uint32_t CLOCK_FREQ = SysCtlClockGet();
    //pin 1= vermelho, pin 2=azul, pin 3=verde

    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_GPIOF) || !SysCtlPeripheralReady(SYSCTL_PERIPH_TIMER0));

    GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, COLOR);

    GPIOPinTypeGPIOInput(GPIO_PORTF_BASE, BUTTON);
    GPIOPadConfigSet(GPIO_PORTF_BASE, BUTTON, GPIO_STRENGTH_4MA, GPIO_PIN_TYPE_STD_WPU);

    IntMasterEnable();
    GPIOIntRegister(GPIO_PORTF_BASE, toggleLED);
    GPIOIntTypeSet(GPIO_PORTF_BASE, BUTTON, GPIO_FALLING_EDGE);
    GPIOIntEnable(GPIO_PORTF_BASE, BUTTON);

    TimerConfigure(TIMER0_BASE, TIMER_CFG_A_ONE_SHOT );
    TimerLoadSet(TIMER0_BASE, TIMER_A, 100000000);
    TimerIntRegister(TIMER0_BASE, TIMER_A, enableButtonInt);
    TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

    while(1){}
    return 0;
}
